import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {TodoListComponent} from "./components/todos/todo-list/todo-list.component";
import {TodoFormComponent} from "./components/todos/todo-form/todo-form.component";
import {TodoFinishedList} from "./components/todos/todo-finished-list/todo-finished-list.component";
import {TodoUnFinishedList} from "./components/todos/todo-unfinished-list/todo-unfinished-list.component";
import {ItemDetail} from "./components/todos/todo-list-item-detail/todo-list-item-detail";

const routes:Routes = [
    {path: '', redirectTo: 'todo-finished-list', pathMatch: 'full'},
    {path: 'todo-finished-list', component: TodoFinishedList},
    {path: 'todo-unfinished-list', component: TodoUnFinishedList},
    {path: 'todo-item-detail/:id', component: ItemDetail}
];


@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule {

}