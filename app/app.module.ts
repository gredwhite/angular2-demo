import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component"
import {TodoListComponent} from "./components/todos/todo-list/todo-list.component";
import {TodoListItemComponent} from "./components/todos/todo-list-item/todo-list-item.component";
import {TodoFormComponent} from "./components/todos/todo-form/todo-form.component";
import { InMemoryWebApiModule } from 'angular2-in-memory-web-api';
import { TodoSeedData } from './components/shared/todo.data';
import { TodosComponent } from "./components/todos/todos.component";
import {AppRoutingModule} from "./app-routing.module";
import {TodoFinishedList} from "./components/todos/todo-finished-list/todo-finished-list.component";
import {TodoUnFinishedList} from "./components/todos/todo-unfinished-list/todo-unfinished-list.component";
import {ItemDetail} from "./components/todos/todo-list-item-detail/todo-list-item-detail";
import {TodoSearchComponent} from "./components/todos/todo-search/todo-search.component";


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        InMemoryWebApiModule.forRoot(TodoSeedData),
        AppRoutingModule
    ],
    declarations: [AppComponent, TodoListComponent, TodoListItemComponent, TodoFormComponent, TodosComponent, TodoFinishedList,
        TodoUnFinishedList, ItemDetail, TodoSearchComponent],
    bootstrap: [AppComponent]
})
export class AppModule {
}