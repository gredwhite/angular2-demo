import { Component } from "@angular/core"
//import { HTTP_PROVIDERS} from "@angular/http"; deprecated

import { Todo } from './components/shared/todo.model'
import {TodoService} from "./components/shared/todoService"

@Component({
    moduleId: module.id,
    selector: "app",
    templateUrl: "app.component.html",
    styleUrls: ['app.component.css'],
    providers: [TodoService]
})
export class AppComponent {
    title:string = "Angular 2Do";
}