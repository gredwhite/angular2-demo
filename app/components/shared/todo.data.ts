import { InMemoryDbService } from 'angular2-in-memory-web-api';
export class TodoSeedData implements InMemoryDbService {
    createDb() {
        let todos = [
            {
                title: "Изучить javascript",
                completed: false,
                id: 1,
                description:"some details1"
            },
            {
                title: "Изучить Angular 2",
                completed: true,
                id: 2,
                description:"some details2"
            },
            {
                title: "Изучить Typescript",
                completed: true,
                id: 3,
                description:"some details3"
            },
            {
                title: "Написать приложение",
                completed: false,
                id: 4,
                description:"some details4"
            }
        ];
        return {todos};
    }
}