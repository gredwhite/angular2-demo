import { Injectable, EventEmitter } from "@angular/core";

import { ITodo } from "./todo.model"
import { VERSION } from "@angular/core";
import { Http } from "@angular/http"
import 'rxjs/add/operator/toPromise'
import {Headers, RequestOptions, Response} from "@angular/http";
import {Todo} from "./todo.model";
import { Observable } from 'rxjs';


@Injectable()
export class TodoService {
    private apiUrl = 'api/todos';

    public itemUpdated$:EventEmitter<ITodo>;

    getTodos():Promise<ITodo[]> {
        return this.http.get(this.apiUrl)
            .toPromise()
            .then(res=>res.json().data)
            .catch(this.handleError);
    }

    getTodo(id:number):Promise<ITodo> {
        return this.http.get(this.apiUrl)
            .toPromise()
            .then(
                res=> {
                    let data = res.json().data;
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].id === id) {
                            return data[i];
                        }
                    }
                    return null;
                }
            )
            .catch(this.handleError);
    }

    addTodo(todo:ITodo):Promise<ITodo> {
        return this.post(todo)
    }

    deleteTodo(todo:ITodo):Promise<ITodo> {
        return this.delete(todo)
    }

    saveTodo(todo:Todo):Promise<ITodo> {
        return this.put(todo)
    }

    search(term:string):Observable<ITodo[]> {
        return this.http
            .get(`${this.apiUrl}?title=${term}`)
            .map((r:Response) => {
                console.log(r);
                console.log(r.json());
                console.log(r.json().data);
                return r.json().data as ITodo[];
            });
    }

    constructor(private http:Http) {
        this.itemUpdated$ = new EventEmitter();
        console.log(VERSION.full)
    }

    private handleError(error:any):Promise<any> {
        console.log("Error occured", error);
        return Promise.reject(error.message || error);
    }

    private post(todo:ITodo):Promise<ITodo> {
        console.log(todo);
        let headers = new Headers({"Content-Type": "application/json"});
        let options = new RequestOptions({headers});
        let body = JSON.stringify(todo);
        return this.http.post(this.apiUrl, body, options)
            .toPromise()
            .then(res=>res.json().data)
            .catch(this.handleError);
    }

    private delete(todo:ITodo):Promise<ITodo> {
        console.log(todo);
        let headers = new Headers({"Content-Type": "application/json"});
        let options = new RequestOptions({headers});

        let url = `${this.apiUrl}/${todo.id}`;
        return this.http.delete(url, options)
            .toPromise()
            .then(res=>todo)
            .catch(this.handleError);
    }

    private put(todo:ITodo):Promise<ITodo> {
        console.log(todo);
        let headers = new Headers({"Content-Type": "application/json"});
        let options = new RequestOptions({headers});
        let body = JSON.stringify(todo);
        let url = `${this.apiUrl}/${todo.id}`;

        return new Promise(resolve=> {
            this.http.put(url, body, options)
                .toPromise()
                .then(res=> {
                    this.itemUpdated$.emit(todo);
                    resolve(todo);

                })
                .catch(this.handleError);
        });
    }
}