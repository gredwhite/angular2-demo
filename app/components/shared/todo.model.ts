export interface ITodo {
    title:string;
    completed: boolean;
    id:number;
    description:string;
}

export class Todo implements ITodo {
    id:number;

    constructor(public title:string,
                public completed:boolean = false,
                public description:string = "default description") {
    }
}