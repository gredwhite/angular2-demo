import {Component, OnInit} from "@angular/core";

import {TodoFormComponent} from "./todo-form/todo-form.component";
import {TodoListComponent} from "./todo-list/todo-list.component";
import { ITodo} from "../shared/todo.model";
import {TodoService} from "../shared/todoService";

@Component({
    moduleId: module.id,
    selector: "todos",
    templateUrl: "todos.component.html",
    styleUrls: ["todos.component.css"]
})
export class TodosComponent implements OnInit {
    todos:ITodo[] = [];

    constructor(private todoService:TodoService) {
    }

    ngOnInit():void {
        this.todoService.getTodos().then(todos => this.todos = todos);
    }

    onTodoCreated(todo:ITodo):void {
        this.todoService.addTodo(todo).then(todo=>this.addToCache(todo));
    }

    onTodoUpdated(todo:ITodo):void {
        this.todoService.saveTodo(todo).then(todo=>this.updateCache(todo));
    }

    onTodoDeleted(todo:ITodo):void {
        this.todoService.deleteTodo(todo).then(todo=>this.deleteFromCache(todo));
    }

    addToCache(todo:ITodo) {
        this.todos.push(todo);
    }

    updateCache(todo:ITodo) {
        this.todos = this.todos.map(function (todoLocal:ITodo) {
            if (todoLocal.id === todo.id) {
                todoLocal.completed = todo.completed;
                todoLocal.title = todo.title;
            }
            return todoLocal;
        })
    }

    deleteFromCache(todo:ITodo) {
        this.todos = this.todos.filter(function (localTodo) {
            return localTodo.id !== todo.id;
        });
    }
}