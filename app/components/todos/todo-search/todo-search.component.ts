import { Component,OnInit } from "@angular/core";
import { TodoService } from "../../shared/todoService";
import { Todo } from "../../shared/todo.model";
import { Observable }        from 'rxjs/Observable';
import { Subject } from "rxjs/Subject";
import {ITodo} from "../../shared/todo.model";
import "../../../rxjs-extensions"

@Component({
    moduleId: module.id,
    selector: "todo-search",
    templateUrl: "todo-search.component.html",
    styleUrls: ["todo-search.component.css"]
})
export class TodoSearchComponent implements OnInit {
    todos:Observable<Todo[]>;
    private searchTerms = new Subject<string>();

    search(term:string) {
        console.log(term);
        this.searchTerms.next(term);
    }

    constructor(private todoService:TodoService) {
    }

    ngOnInit():void {
        this.todos = this.searchTerms
            .debounceTime(200)
            .distinctUntilChanged()
            .switchMap((term)=>term ? this.todoService.search(term) : Observable.of<ITodo[]>([]))
            .catch(error=> {
                console.log(error);
                return Observable.of<ITodo[]>([]);
            });
    }
}