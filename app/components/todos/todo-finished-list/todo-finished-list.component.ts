import {Component, OnInit} from "@angular/core";
import {TodoService} from "../../shared/todoService";
import {ITodo} from "../../shared/todo.model";
import { Router } from "@angular/router"

@Component({
    moduleId: module.id,
    templateUrl: "todo-finished-list.html",
    selector: "todo-finished-list",
    styleUrls: ["todo-finished-list.css"]

})
export class TodoFinishedList implements OnInit {
    finishedTodos:ITodo[];

    constructor(private todoService:TodoService, private router:Router) {
        todoService.itemUpdated$.subscribe((item:ITodo) => this.ngOnInit());

    }

    ngOnInit():void {
        this.todoService.getTodos()
            .then((items:ITodo[])=>this.finishedTodos = items.filter(function (item:ITodo) {
                return item.completed;
            }));
    }

    navigateToDetails(id:number):void {
        console.log(11);
        this.router.navigate(['/todo-item-detail', id]);
    }
}