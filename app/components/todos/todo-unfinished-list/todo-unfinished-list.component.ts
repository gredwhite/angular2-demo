import {Component, OnInit} from "@angular/core";
import {TodoService} from "../../shared/todoService";
import {ITodo} from "../../shared/todo.model";
@Component({
    moduleId: module.id,
    templateUrl: "todo-unfinished-list.html",
    selector: "todo-unfinished-list",
    styleUrls: ["todo-unfinished-list.css"]
})
export class TodoUnFinishedList implements OnInit {
    unfinishedTodos:ITodo[];

    constructor(private todoService:TodoService) {
        todoService.itemUpdated$.subscribe((item:ITodo) => this.ngOnInit());

    }

    ngOnInit():void {
        this.todoService.getTodos()
            .then((items:ITodo[])=>this.unfinishedTodos = items.filter(function (item:ITodo) {
                return !item.completed;
            }));
    }

}