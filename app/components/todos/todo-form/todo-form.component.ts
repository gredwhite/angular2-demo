import {Component, Output, EventEmitter} from "@angular/core";
import {Todo} from "../../shared/todo.model";
import {ITodo} from "../../shared/todo.model";

@Component({
    moduleId: module.id,
    selector: "todo-form",
    templateUrl: "todo-form.component.html",
    styleUrls: ["todo-form.component.css"],
})
export class TodoFormComponent {
    @Output() created:EventEmitter<ITodo> = new EventEmitter<Todo>();

    newTodoTitle:string;

    create():void {
        if (this.newTodoTitle) {
            this.created.emit(new Todo(this.newTodoTitle));
            this.newTodoTitle="";
        }
    }
}