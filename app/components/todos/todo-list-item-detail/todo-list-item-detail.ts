import {Component, OnInit} from "@angular/core";
import {ITodo} from "../../shared/todo.model";
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import 'rxjs/add/operator/switchMap';
import { TodoService } from "../../shared/todoService";

@Component({
    moduleId: module.id,
    selector: "item-detail",
    templateUrl: "todo-list-item-detail.html",
    styleUrls:["todo-list-item-detail.css"]
})
export class ItemDetail implements OnInit {

    todo:ITodo;

    constructor(private todoService:TodoService,
                private activatedRoute:ActivatedRoute,
                private location:Location) {

    }

    ngOnInit():void {
        this.activatedRoute.params
            .switchMap((params:Params) => this.todoService.getTodo(+params['id']))
            .subscribe(todo => {
                this.todo = todo
            });
    }

}