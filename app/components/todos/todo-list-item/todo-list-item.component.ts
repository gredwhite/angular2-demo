/**
 * Created by ntkachev on 12/13/16.
 */
import {Component, Input, Output, EventEmitter} from "@angular/core"
import {Todo} from "../../shared/todo.model"

@Component({
    moduleId: module.id,
    selector: "todo-list-item",
    templateUrl: "todo-list-item.component.html",
    styleUrls: ["todo-list-item.component.css"]
})
export class TodoListItemComponent {
    @Input() todo:Todo;
    @Output() deleted = new EventEmitter();
    @Output() toggled = new EventEmitter();

    toggle(todo:Todo):void {
        this.todo.completed = !this.todo.completed;
        this.toggled.emit(todo);
    }

    delete():void {
        this.deleted.emit(this.todo);
    }

}