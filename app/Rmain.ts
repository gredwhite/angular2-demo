import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { XHRBackend } from "@angular/http";
import { InMemoryBackendService, SEED_DATA } from "angular2-in-memory-web-api";

import { AppModule } from "./app.module";
import { TodoSeedData } from "./components/shared/todo.data";

const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule, [
    {provide: XHRBackend, useClass: InMemoryBackendService},
    {provide: SEED_DATA, useClass: TodoSeedData}
]);